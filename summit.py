#!/mnt/miniconda/envs/fraud_ip/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals, absolute_import, division, print_function

from cron_utils.args import generate_args
from cron_utils.args import sub

from common import default_parser, local_class, submit_job

def main(host, status, priority, retry, pool, group, stage, extra_args):

    file_name = extra_args["script"]
    class_name = local_class(file_name)
    job_name = sub('[fraud_ip] {file_name}')

    inputs = []
    outputs = []

    args = dict(group=group)

    title = '' % ()

    cmd_args = generate_args()

    submit_job(
        class_name,
        job_name,
        title,
        status,
        priority,
        retry,
        inputs=inputs,
        outputs=outputs,
        host=host,
        cmd_args=cmd_args,
        args=args,
        pool=pool,
        group=group,
        stage=stage)


if __name__ == '__main__':
    parser = default_parser()
    parser.add_argument('--script', required=True)
    args = parser.parse_args()

    main(
        host=args.host,
        status=args.status,
        priority=args.priority,
        retry=args.retry,
        pool=args.pool,
        group=args.group,
        stage=args.stage,
        extra_args={
            "script": args.script
        }
    )
