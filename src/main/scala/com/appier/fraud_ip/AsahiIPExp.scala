package com.appier.fraud_ip

import org.apache.log4j.LogManager
import org.apache.spark.storage.StorageLevel._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.SaveMode

import org.rogach.scallop._

import org.apache.spark.sql.functions._
import org.apache.spark.sql._
import org.apache.spark.sql.expressions.Window

import org.apache.spark.sql.types.IntegerType

//Fraud IP, Asahi IP實驗
//我們希望找到哪些IP是fraud的,但其實所謂fraud沒有一個好的衡量標準,因此有了這個Asahi IP實驗
//Asahi有給我們一包reginal的ip包,例如鹿兒島ip包
//Aixon基於某些原因必須用某些方法對這ip包吐一包idfa包出來,而這方法理論上就是看用過這些ip的有哪些idfa
//而Aixon又會對每個idfa給個地區,而這個給法應該就是看這idfa有用過哪些ip,而這些ip可以去查maxmind db,之後大概就取個majority之類的
//此時就會發現,鹿兒島ip包內的ip查maxmind db可發現的確鹿兒島佔大多數,但透過Aixon轉成idfa又取得這些idfa的地區後,卻變成了東京佔大多數
//這理論上是完全可能發生的,因為用過這鹿兒島ip的idfa不表示majority就是使用鹿兒島ip,可能大多是用東京ip
//可能性有很多,可能這idfa就真的去鹿兒島一下子又回到東京之類的
//但理想上,這個實驗假設為常使用鹿兒島ip的idfa就應該是在鹿兒島的device,majority就應該是鹿兒島的ip,之所以會有那麼多的東京ip是因為這些ip是fraud
//因此只要我們篩掉這些我們認為是fraud的ip後,發現鹿兒島比例提高了,就表示我們的篩法不錯,真的是fraud
//先不管這對於fraud的假設到底對不對,反正這實驗就是這樣
object AsahiIPExp {
    val logger = LogManager.getLogger(this.getClass.getSimpleName)

    val taskName = this.getClass.getSimpleName.init

    def main(args: Array[String]) {
        object conf extends ScallopConf(args) {
            val title = opt[String]()
            verify()
        }

        val optTitle = conf.title.toOption
        val title = (Option(taskName) ++ optTitle).mkString(" ")

        val sparkConf = JobUtil.defaultConf(title)
        val spark = SparkSession
            .builder()
            .config(sparkConf)
            .getOrCreate()
        import spark.implicits._

        //asahi提供的的原始ip包
        //7C54是鹿兒島包,裡面ip查maxmind得到的city比例前幾名為
        //Kagoshima 34% Tokyo 14% Miyazaki 8% Fukuoka 4% Kitakyushu 4% Osaka 4% Miyakonojo 3%, 大多數是鹿兒島沒錯
        /*
        val asahi_ip = spark.read
            .format("csv")
            .option("header", "true")
            .load("s3a://appier-cd-tv-asahi/query/query_master_table/date=20190128/7C54/in/")
            .select(
                $"ip_address" //非空
            )
        */

        //在某種邏輯之下用asahi提供的ip包生出來的對應的idfa包
        //https://appier.atlassian.net/wiki/spaces/CST/pages/370248074/TV+Project#TVProject-AixonIPCRM 但好像根本沒寫怎麼生出來的
        //有一個idfa對應多個ip的情況,也有一個ip對應多個idfa的情況(對應的數量大約都是1對1~5左右這種數字)
        //取法估計就是有用過這個ip的idfa就放進去這樣,所以會多idfa對多ip,可能再加個threshold等等的機制
        val asahi_idfa = spark.read
            .format("csv")
            .option("header", "true")
            .load("s3a://appier-cd-tv-asahi/query/query_master_table/date=20190128/7C34/aixon_crm/")
            .select(
                //customuid和idfa為多對多關係
                $"customuid", //非空
                $"idfa" //非空
            )
            .groupBy($"idfa")//這邊目標是取asahi的idfa,每個idfa唯一即可,因此group
            .count()
            .select(
                $"idfa" //非空
            )

        //Aixon會給每個idfa一個region,具體判定邏輯不清楚
        //但我理解上是若在implog中發現一個idfa有某個ip的imp,就判定有用過某個ip,而ip是地區綁定,可查maxmind db得到region
        //之後再看看此idfa大部分是使用哪個地區的ip就判定是哪個地區
        //此實驗不去向Aixon要,而是自己還原這個過程,因此這邊讀入implog
        //另外就是implog裡面lat lon city country等等資訊都是直接用該ip從maxmind db裡面查出來的,直接取用就好不用自己再查
        val imp_fill_ip = spark.read
            .parquet(List("s3a://appier-cd-imp/out/imp_fill_ip/date=20190128"): _*)
            .select(
                $"adid", //非空
                $"idfa", //可空
                $"ip", //可空
                $"lat", //可空
                $"lon", //可空
                $"city", //可空
                $"country", //可空
                $"timestamp", //非空
                $"time" //非空
            )
            .cache()

        //asahi idfa包的地區分佈(地區判定是採用和Aixon依樣的判定法,從implog算出)
        //之後會有篩過fraud ip之後的implog算出來的asahi idfa包地區分佈,可互相比較
        val asashi_idfa_city_percentage = imp_fill_ip
            .join(asahi_idfa, Seq("idfa")) //只需要asahi idfa包裡有的就可,同時可去空的idfa
            .filter($"city" !== "") //city可空,我們地區定義是city,空的不知道city,因此不拿去判定,篩掉
            .groupBy($"idfa", $"city")//理論上是要group ip, ip再拿去查city,但implog已經幫我們查好,ip city又是一對一對應,因此直接group city
            .count()
            .withColumn(
                "max_count", 
                max($"count")
                    .over(
                        Window.partitionBy($"idfa")
                    )
            )
            .filter($"count" === $"max_count")//取majority
            .select(
                $"idfa", 
                $"city"
            )
            .groupBy($"city")
            .count()
            .withColumn("percentage", $"count" /  sum($"count").over())

        //來用一些條件篩選implog,做出個fraud ip list
        //篩選時用全部的implog來做,並非只用asahi idfa包內idfa的implog來做,否則這整個fraud ip list就根本是只針對這個實驗了
        //理想上該實驗本質應該是找出fraud ip,而非把asahi ip包的地區distribution調到如我們所想,調出想要的distribution只是驗證真的是fraud的方法
        //我知道這整個實驗很假設性,但先不管

        //從asahi statistic diff發現,那些因為用過asahi鹿兒島ip包中的鹿兒島ip,而被Aixon放入asahi鹿兒島idfa包的idfa
        //其使用東京ip的implog會有和同idfa上一個imp時間間格,相對於其使用鹿兒島ip的implog而言,特別短的現象發生
        //因此可以用做篩選條件
        val ip_fraud = imp_fill_ip 
            .select(
                $"adid", //非空
                $"ip", //可空
                $"time".cast(IntegerType) as "time" //非空
            )
            .filter($"ip" !== "") //空的ip不放入fraud ip表
            .withColumn(
                "time_delta", 
                $"time" - lag($"time", 1)
                    .over(
                        Window.partitionBy($"adid") //取lag時用adid當單位,用idfa當單位也可以,但記得篩掉idfa是空的,否則空的太多會大量集中,導致shuffle fail
                            .orderBy($"time")
                    )
            )
            .select(
                $"adid", //非空
                $"ip", //非空
                $"time_delta" //可null (沒同adid的前一個)
            )
            .filter(
                $"time_delta".isNotNull //去null,無法判定的就不加入判定
            )
            .filter($"time_delta" <= 200) //200秒以下當成是fraud的可能性
            .groupBy($"ip")
            .count()
            .filter($"count" >= 50) //一個ip有50個200秒以下的implog,就當fraud
            .select(
                $"ip" as "fraud_ip"
            )

        val asashi_idfa_city_filtered_percentage = imp_fill_ip
            .join(asahi_idfa, Seq("idfa"))
            .filter($"city" !== "")
            .join(
                ip_fraud, 
                $"ip" === $"fraud_ip", 
                "left_outer"
            )
            .filter($"fraud_ip".isNull)
            .groupBy($"idfa", $"city")
            .count()
            .withColumn(
                "max_count", 
                max($"count")
                    .over(
                        Window.partitionBy($"idfa")
                    )
            )
            .filter($"count" === $"max_count")
            .select(
                $"idfa", 
                $"city"
            )
            .groupBy($"city")
            .count()
            .withColumn("percentage", $"count" /  sum($"count").over())

        asashi_idfa_city_percentage
            .join(
                asashi_idfa_city_filtered_percentage
                    .select(
                        $"city", 
                        $"count" as "count_filtered", 
                        $"percentage" as "percentage_filtered"
                    ), 
                Seq("city")
            )
            .orderBy($"count".desc)
            .show(1000, false)
    }
}
