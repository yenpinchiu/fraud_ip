package com.appier.fraud_ip

import org.apache.log4j.LogManager
import org.apache.spark.storage.StorageLevel._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.SaveMode

import org.rogach.scallop._

import org.apache.spark.sql.functions._
import org.apache.spark.sql._
import org.apache.spark.sql.expressions.Window

import org.apache.spark.sql.types.IntegerType

//這份code的目的是分析從增加asahi ip實驗中目標地區比例的角度來看,要怎麼做
//具體來說就是比方,今天我們拿到一包asahi鹿兒島ip包,裡面有36% ip從maxmind db查時是鹿兒島
//今天Aixon透過這個ip包得到一個idfa包,若某個idfa是因為使用了asahi ip包中某個鹿兒島ip,而被放入,則在理想情況上該idfa應該被Aixon判定為鹿兒島idfa
//若因為它有很多ip為東京的implog,使得他被判定為東京,則他就會使的這個Aixon輸出的idfa包中鹿兒島比例降低
//代表如果他從implog的角度來看,該idfa使用的東京ip都是我們篩掉的目標,因為篩掉我們就能使他更容易被判定成鹿兒島,使的distritution更接近原始的asahi地區ip包
//所以不如就把這種ip都抓來看看有什麼性質
object AsahiStatisticDiff {
    val logger = LogManager.getLogger(this.getClass.getSimpleName)

    val taskName = this.getClass.getSimpleName.init

    def main(args: Array[String]) {
        object conf extends ScallopConf(args) {
            val title = opt[String]()
            verify()
        }

        val optTitle = conf.title.toOption
        val title = (Option(taskName) ++ optTitle).mkString(" ")

        val sparkConf = JobUtil.defaultConf(title)
        val spark = SparkSession
            .builder()
            .config(sparkConf)
            .getOrCreate()
        import spark.implicits._

        //跑asahi_idfa_city.py產生的idfa_city.csv
        //將asahi idfa中每個idfa對應的asahi ip都拿去查maxmind db(有查到就放,沒有就丟掉)得到位置
        //意義上就是該idfa,是因為用過什麼地區的ip,而被放進asahi idfa list
        val asashi_idfa_city = spark.read
            .format("csv")
            .option("header", "true")
            .load("s3a://appier-cd-imp/tmp/idfa_city.csv")
            .groupBy($"idfa", $"city") //idfa可能對應多個ip,有多個city的話就取majority
            .count()
            .withColumn(
                "idx", 
                row_number()
                    .over(
                        Window
                        .partitionBy("idfa")
                        .orderBy($"count".desc)
                    )
            )
            .filter($"idx" === 1) //這邊直接取最多的,同數量就隨機(不確定spark的orderby是穩定還是不穩定sort)
            .select(
                $"idfa", //非空
                $"city" as "output_reason_city" //非空
            )

        val imp_fill_ip = spark.read
            .parquet(List("s3a://appier-cd-imp/out/imp_fill_ip/date=20190128"): _*)
            .select( 
                $"idfa", //可空
                $"ip", //可空
                $"city", //可空
                $"time" //非空
            )

        val compare = imp_fill_ip
            .filter($"ip" !== "")//比較的metric都會以ip為單位,並且不需要空的,因此這邊去掉空的
            .join(asashi_idfa_city, Seq("idfa"))//join過之後自動去掉idfa為空
            .select(
                $"idfa", //非空
                $"ip", //非空
                $"city", //可空
                $"output_reason_city", //非空
                $"time".cast(IntegerType) as "time" //非空
            )
            .withColumn(
                "time_delta", 
                $"time" - lag($"time", 1)
                    .over(
                        Window.partitionBy($"idfa")
                            .orderBy($"time")
                    )
            )
            .withColumn(
                "pre_city", 
                lag($"city", 1)
                    .over(
                        Window.partitionBy($"idfa")
                            .orderBy($"time")
                    )
            )
            .select(
                $"idfa", //非空
                $"ip", //非空
                $"city", //可空
                $"output_reason_city", //非空
                $"time_delta", //可null
                $"pre_city" //可null, 可空
            )
            .cache()

        //這些implog的ip非鹿兒島,但這些idfa是因為用過鹿兒島ip而被放入asahi idfa list,表示這些implog對鹿兒島的比例有負面影響
        compare
            .filter($"time_delta".isNotNull)
            .filter($"city" !== "")
            .filter(($"city" !== "kagoshima") && ($"output_reason_city" === "Kagoshima"))
            .groupBy($"ip")
            .agg(
                avg($"time_delta") as "time_delta" //每個ip所有imp的平均time_delta
            )
            .groupBy()
            .agg(
                avg($"time_delta") as "time_delta" //所有ip的平均time_delta平均
            )
            .show(10, false)

        compare
            .filter($"pre_city".isNotNull)
            .filter(($"city" !== "kagoshima") && ($"output_reason_city" === "Kagoshima"))
            .filter(($"city" !== "") && ($"pre_city" !== ""))
            .select(
                $"ip", 
                $"city" === $"pre_city" as "city_no_change"
            )
            .groupBy($"ip", $"city_no_change")
            .count()
            .withColumn("percentage", $"count" /  sum($"count").over(Window.partitionBy($"ip")))
            .filter($"city_no_change" === false)
            .groupBy()
            .agg(
                avg($"percentage") as "percentage"
            )
            .show(10, false)
        
        //這些implog的ip是鹿兒島,而這些idfa是因為用過鹿兒島ip而被放入asahi idfa list,表示這些implog對鹿兒島的比例有正面影響
        compare
            .filter($"time_delta".isNotNull)
            .filter($"city" !== "")
            .filter(($"city" === "kagoshima") && ($"output_reason_city" === "Kagoshima"))
            .groupBy($"ip")
            .agg(
                avg($"time_delta") as "time_delta"
            )
            .groupBy()
            .agg(
                avg($"time_delta") as "time_delta"
            )
            .show(10, false)
        
        compare
            .filter($"pre_city".isNotNull)
            .filter(($"city" === "kagoshima") && ($"output_reason_city" === "Kagoshima"))
            .filter(($"city" !== "") && ($"pre_city" !== ""))
            .select(
                $"ip", 
                $"city" === $"pre_city" as "city_no_change"
            )
            .groupBy($"ip", $"city_no_change")
            .count()
            .withColumn("percentage", $"count" /  sum($"count").over(Window.partitionBy($"ip")))
            .filter($"city_no_change" === false)
            .groupBy()
            .agg(
                avg($"percentage") as "percentage"
            )
            .show(10, false)
    }
}
