import geoip2.database

if __name__ == "__main__":
    ip_city_reader = geoip2.database.Reader('GeoIP2-City.mmdb')
    asahi_ip_to_aixon_idfa = open("part-00000-7db21bcb-22be-4917-b077-d9cc5ab23ee0-c000.csv", 'r', encoding='utf-8')
    idfa_city = open("idfa_city.csv", 'w', encoding='utf-8')

    idfa_city.write("idfa,city\n")
    columns = next(asahi_ip_to_aixon_idfa) #column name
    for line in asahi_ip_to_aixon_idfa:
        line_split = line.strip().split(",")
        customuid = line_split[0]
        idfa = line_split[1]

        response = ip_city_reader.city(customuid)
        if len(response.city.names) != 0:
            idfa_city.write("{},{}\n".format(idfa, response.city.names["en"]))

    asahi_ip_to_aixon_idfa.close()
    ip_city_reader.close()
    idfa_city.close()
