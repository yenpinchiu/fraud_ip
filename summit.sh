#!/bin/bash

make setup
make package
make build
make install

virtualenv manually_summit_env
source manually_summit_env/bin/activate
pip install smart_open==1.8.0 #cron_utils的requirement內smart_open pip裝時最新的1.8.1(2019/4/9上的版本)會失敗,可裝1.8.0版繞過去,但直接改cron_utils的requirement又很麻煩,因此這邊先直接裝繞過去
pip install -r requirements.txt

SCRIPT=${1}
python summit.py --script $SCRIPT

deactivate
rm -rf manually_summit_env
